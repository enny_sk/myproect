package services;


import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertTrue;

/**
 * Created by enny on 04.09.17.
 */
public class CustomService {
    private WebDriver driver;
    Random random = new Random();

    public CustomService(WebDriver driver) {
        this.driver = driver;
    }

    public void waitClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }


    public void waitUrl(String url) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void waitUrl2(String url) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until((WebDriver webDriver) -> driver.getCurrentUrl().contains(url));
    }

    public void switchToLastWindow() {
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }

    public void goTo(String url) {
        driver.get(url);
    }


    public static void waitForCookie(final String cookieName, final WebDriver driver){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(new ExpectedCondition<Boolean>(){
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.manage().getCookieNamed(cookieName) != null);
                }
            });
        }catch (TimeoutException e){
            assertTrue(true, "was not found after timeout");
        }
    }
    public void waitForElementVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 5000);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void clickOnElement(WebElement element, String elementName) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException ex) {
            return;
        }
        try {
            element.click();
        } catch (NoSuchElementException e) {
            assertTrue(false, "\"" + elementName + "\" not found on page after timeout");
        } catch (StaleElementReferenceException e) {
            element.click();
        }
    }



    public void chooseRandomElementInList(List<WebElement> list) {
        if(list.isEmpty()){
            Log.warn("Template hasn't lists");
            return;
        }
        int numberOnTemplate = random.nextInt(list.size());
        this.waitClickable(list.get(numberOnTemplate));
        this.clickOnElement(list.get(numberOnTemplate), "Random element");
        this.waitClickable(list.get(numberOnTemplate));
    }
}
