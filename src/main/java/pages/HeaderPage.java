package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;

/**
 * Created by enny on 04.09.17.
 */
public class HeaderPage {
    private final WebDriver driver;
    private CustomService service;

    public HeaderPage(WebDriver driver) {
        this.driver = driver;
        service = new CustomService(driver);
    }

    @FindBy(id = "menu-favorites")
    public WebElement headerHeart;

    public void waitForHeartEnable(){
        service.waitClickable(headerHeart);
    }
}
