package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;
import services.Log;

import java.util.List;


/**
 * Created by enny on 04.09.17.
 */
public class TemplatePage {
    private final WebDriver driver;
    private CustomService service;

    public TemplatePage(WebDriver driver) {
        this.driver = driver;
        service = new CustomService(driver);
    }


    @FindBy(xpath = ".//*[contains(@class, 'add-to-cart')]//*[contains(@class, 'js-button-add-to-cart')]")
    private WebElement buttonAddTemplateToCart;

    @FindBy(xpath = ".//*[contains(@class, 'checkout-button')]//*[contains(@class, ' js-checkout-button')]")
    private WebElement checkoutButton;

    @FindBy(xpath = ".//*[contains(@class, 'js-total-price')]")
    private WebElement orderTotal;

    @FindBy(xpath = ".//*[contains(@id, 'cart-popup' )]//*[contains(@class, 'cart-summary-item')]//*[contains(@class, 'offer-')]")
    private WebElement onTemplateOfferInSummary;

    @FindBy(xpath = ".//*[contains(@class, 'item-onCartOffer')]")
    private WebElement onCartOfferInSummary;

    @FindBy(xpath = ".//*[@class='template-shopping-options upsells']//ul//li//*[contains(@id, 'sc-add-offer-oncart')]")
    public List<WebElement> offerOnTemplateList;

    @FindBy(xpath = ".//*[@class='template-shopping-options recommended']//ul//li//*[contains(@id, 'sc-add-offer')]")
    public List<WebElement> offerOnCartList;


    public void clickAddToCartTemplate() {
        service.waitClickable(buttonAddTemplateToCart);
        buttonAddTemplateToCart.click();
    }

    public void selectRandomOffer(String offerType){
        service.waitClickable(checkoutButton);
        service.waitForElementVisible(orderTotal);
        switch (offerType){
            case "OnTemplate":
                service.chooseRandomElementInList(offerOnTemplateList);
                Log.info("Selected random OnTemplate offer");
                break;
            case "OnCart":
                service.chooseRandomElementInList(offerOnCartList);
                Log.info("Selected random OnCart offer");
                break;
            default:
                throw new IllegalArgumentException("Incorrect offer name, choose OnCart or OnTemplate offer name");
        }
    }

    public void clickOnCheckoutButton() {
        service.waitForElementVisible(onTemplateOfferInSummary);
        service.waitForElementVisible(onCartOfferInSummary);
        service.waitClickable(checkoutButton);
        checkoutButton.click();
        Log.info("Go to checkout");
    }


}

