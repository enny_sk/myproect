package services;

/**
 * Created by enny on 06.09.17.
 */
public enum  Payment {
    PayPal,
    Stripe,
    Skrill,
    PayProGlobal
}
