package testSuietes;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CheckoutPage;
import pages.HeaderPage;
import pages.TemplatePage;
import properties.User;
import services.CustomService;
import services.Log;
import services.Payment;

/**
 * Created by enny on 04.09.17.
 */
public class PurchaseWithNewUserTest extends BaseTest {
    @Test
    void testBuy() {
        User user = new User();
        CustomService service = new CustomService(driver);

        service.goTo("https://www.templatemonster.com/wordpress-themes/47925.html");
        Log.info("Go to preview page");
        HeaderPage headerPage = PageFactory.initElements(driver, HeaderPage.class);
        TemplatePage templatePage = PageFactory.initElements(driver, TemplatePage.class);

        headerPage.waitForHeartEnable();
        Log.info("Preview page Loaded");
        templatePage.clickAddToCartTemplate();
        headerPage.waitForHeartEnable();
        Log.info("Open popup shopping cart");
        templatePage.selectRandomOffer("OnTemplate");
        templatePage.selectRandomOffer("OnCart");
        templatePage.clickOnCheckoutButton();
        service.switchToLastWindow();
        service.waitUrl("checkout");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.templatemonster.com/checkout.php", "Incorrect url");


        CheckoutPage checkoutPage = PageFactory.initElements(driver, CheckoutPage.class);
        checkoutPage.registrationNewUser(user);
        CustomService.waitForCookie("access_token", driver);
        Assert.assertFalse(driver.manage().getCookieNamed("access_token").getValue().isEmpty(), "Incorrect cookie value.");

        checkoutPage.payByPaymentMethod(Payment.PayPal);
        service.switchToLastWindow();
        service.waitUrl("pay");
        Log.info("The payment was opened correctly");
    }
}





