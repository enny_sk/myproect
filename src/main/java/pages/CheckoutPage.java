package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import properties.User;
import services.CustomService;
import services.Log;
import services.Payment;

import java.util.List;


/**
 * Created by enny on 04.09.17.
 */
public class CheckoutPage {
    private final WebDriver driver;
    private CustomService service;

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
        service = new CustomService(driver);
    }

    //Sign In.

    @FindBy(xpath=".//*[contains(@id,'signin3-form-email')]")
    public WebElement emailFild;

    @FindBy(xpath = ".//*[contains(@id,'signin3-new-customer')]")
    public WebElement continueButton;

    //Billing Information.

    @FindBy(xpath = ".//*[contains(@id,'billinginfo3-form-fullname')]")
    public WebElement fullName;

    @FindBy(id = "billinginfo3-form-phone")
    public WebElement phoneNumber;

    @FindBy(id = "billinginfo3-form-postalcode")
    public WebElement postCode;

    @FindBy(xpath = "//fieldset//div[4]//*[contains(@class, 'rd-field-success')]//span")
    public WebElement activeIconFildPostalCode;

    @FindBy(id = "billinginfo3-form-cityname" )
    public WebElement cityName;

    @FindBy(id = "billinginfo3-form-submit")
    public WebElement continuePaymentButton;

    //Payment methods.

    @FindBy(xpath = "//*[contains(@id,'checkout-payment-buy')]")
    public List<WebElement> paymentList;


    public void enterEmail(String email) {
        emailFild.sendKeys(email);
        Log.info("Enter email");
    }

    public void enterFullName(String name){
        fullName.sendKeys(name);
        Log.info("Enter name");
    }

    public void enterPhoneNumber(String phone){
        phoneNumber.sendKeys(phone);
        Log.info("Enter phone");
    }

    public void enterPostCode(String zip){
        postCode.sendKeys(zip);
        Log.info("Enter zip");
    }

    public void enterCity(String city){
        cityName.sendKeys(city);
        Log.info("Enter city");
    }

    public void registrationNewUser (User user){
        service.waitClickable(emailFild);
        enterEmail(user.getEmail());
        continueButton.click();
        Log.info("Click on Continue button");
        enterFullName(user.getName());
        enterPhoneNumber(user.getPhone());
        enterPostCode(user.getZip());
        service.waitForElementVisible(activeIconFildPostalCode);
        enterCity(user.getCity());
        service.waitForElementVisible(continuePaymentButton);
        continuePaymentButton.click();
        Log.info("Click on Continue payment button");
    }

    public void payByPaymentMethod(Payment paymentMethod){
        service.waitClickable(paymentList.get(1));
        WebElement payment = driver.findElement(By.id("checkout-payment-buy-" + paymentMethod));
        service.clickOnElement(payment, paymentMethod + " button");
        Log.info("Pay via "+paymentMethod);
    }
}
