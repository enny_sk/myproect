package testSuietes;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import services.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by enny on 04.09.17.
 */
public class BaseTest {
    public WebDriver driver;

    @BeforeSuite
    public void startSuite() {
        Log.info("Start suite");
    }


    @BeforeMethod
    void init() throws MalformedURLException {
        Log.info("Start test");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        Log.info("Test run in  google chrome.");
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Log.info("Maximize window.");
    }

    @AfterMethod
    void clear() {
        driver.quit();
        Log.info("Test finished");
    }


    @AfterSuite
    public void afterSuite() {
        Log.info("Suite finished");
    }
}
