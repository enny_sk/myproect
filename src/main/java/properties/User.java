package properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by enny on 04.09.17.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor

public class User {
    private String name = "Enny Tester";
    private String email = "enny-"+System.currentTimeMillis()+"@templatemonster.me";
    private String phone ="0984280470";
    private String zip ="ggg";
    private String city ="Test";

   }
